const { data } = require('./data.js');
const minimist = require('minimist');

const { filter, count } = minimist(process.argv.slice(2));

if (filter) {
  let filtredList = [];
  try {
    data.forEach((item) => {
      let peopleList = [];
      item.people?.forEach((people) => {
        let animalsList = [];
        people.animals?.forEach((animal) => {
          if (animal.name.includes(filter)) {
            animalsList.push(animal);
          }
        });
        animalsList.length &&
          peopleList.push({ ...people, animals: animalsList });
      });
      peopleList.length && filtredList.push({ ...item, people: peopleList });
    });
    filtredList.length && console.log(JSON.stringify(filtredList, null, 4));
    if (filtredList.length) {
      return filtredList;
    }
  } catch (error) {
    console.error('an error has occured', error);
  }
}

if (count) {
  try {
    data.forEach((item, indexItem) => {
      data[indexItem].name = `${item?.name} [${item?.people.length}]`;
      item.people?.forEach((people, indexPeople) => {
        data[indexItem].people[
          indexPeople
        ].name = `${people?.name} [${people?.animals.length}]`;
      });
    });
    console.log(JSON.stringify(data, null, 4));
    return data;
  } catch (error) {
    console.error('an error has occured! ', error);
  }
}
